#include "../include/bmp_io.h"

const char* read_msg[] = {
    [BMP_READ_OK] = "Successfully read the file %s",
    [BMP_READ_ARG_NULL] = "One of the arguments is NULL",
    [BMP_READ_INVALID_HDR] = "Couldn't read BMP header of file %s",
    [BMP_READ_OUT_OF_MEM] = "Couldn't read BMP file %s - out of memory",
    [BMP_READ_DATA_CORRUPT] = "Couldn't read BMP file %s - corrupted data"};

const char* write_msg[] = {
    [BMP_WRITE_OK] = "Successfully written BMP to file %s",
    [BMP_WRITE_ARG_NULL] = "One of the arguments is NULL",
    [BMP_WRITE_OUT_OF_SPACE] =
        "Couldn't write BMP to file %s - not enough disk space"};

void bmp_print_read_status(enum read_status status, char* file_name) {
    FILE* out = status == BMP_READ_OK ? stdout : stderr;
    fprintf(out, read_msg[status], file_name);
    fprintf(out, "\n");
}

void bmp_print_write_status(enum write_status status, char* file_name) {
    FILE* out = status == BMP_WRITE_OK ? stdout : stderr;
    fprintf(out, write_msg[status], file_name);
    fprintf(out, "\n");
}

#include "../include/bmp_io.h"
#include "../include/transform.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        return -1;
    }

    char* source_img_name = argv[1];
    char* transformed_img_name = argv[2];

    FILE* source_img_file = fopen(source_img_name, "rb");
    if (source_img_file == NULL) {
        fprintf(stderr, "File %s not found\n", source_img_name);
        return -1;
    }

    FILE* transformed_img_file = fopen(transformed_img_name, "wb");
    if (transformed_img_file == NULL) {
        fprintf(stderr, "File %s not found\n", transformed_img_name);
        fclose(source_img_file);
        return -1;
    }

    struct image source_img = {0};
    enum read_status read_status = from_bmp(source_img_file, &source_img);
    bmp_print_read_status(read_status, source_img_name);
    if (read_status != BMP_READ_OK) {
        fclose(source_img_file);
        fclose(transformed_img_file);
        return -1;
    }

    struct image transformed_img = rotated_img(source_img);
    if (transformed_img.data == NULL) {
        fprintf(stderr, "Failed to rotate image - out of memory");
        fclose(source_img_file);
        fclose(transformed_img_file);
        return -1;
    }

    enum write_status write_status = to_bmp(transformed_img_file, &transformed_img);
    bmp_print_write_status(write_status, transformed_img_name);
    if (write_status != BMP_WRITE_OK) {
        fclose(source_img_file);
        fclose(transformed_img_file);
        return -1;
    }

    free_image(&source_img);
    free_image(&transformed_img);

    printf("Success! Rotated image is located at %s\n", transformed_img_name);
    
    fclose(source_img_file);
    fclose(transformed_img_file);

    return 0;
}

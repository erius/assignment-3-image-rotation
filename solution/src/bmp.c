#include "../include/bmp.h"

#include <stdbool.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t row_padding(uint64_t width) {
    return BMP_PADDING - (width * sizeof(struct pixel)) % BMP_PADDING;
}

static struct bmp_header create_bmp_header(uint64_t width, uint64_t height) {
    uint32_t image_size = height * (width * sizeof(struct pixel) + row_padding(width));
    return (struct bmp_header) {
        .bfType = BMP_TYPE,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = BMP_RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_INFO_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BITS_COUNT,
        .biCompression = BMP_NO_COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = BMP_PPM,
        .biYPelsPerMeter = BMP_PPM,
        .biClrUsed = BMP_CLR,
        .biClrImportant = BMP_CLR
    };
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL || img == NULL) return BMP_READ_ARG_NULL;
    struct bmp_header header;
    size_t header_read = fread(&header, sizeof(struct bmp_header), 1, in);
    if (header_read != 1) return BMP_READ_INVALID_HDR;
    if (header.biWidth <= 0 || header.biHeight <= 0) return BMP_READ_INVALID_HDR;
    *img = create_image(header.biWidth, header.biHeight);
    if (img->data == NULL) return BMP_READ_OUT_OF_MEM;
    uint8_t padding = row_padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        struct pixel* pixel_row = &img->data[y * img->width];
        size_t pixels_read = fread(pixel_row, sizeof(struct pixel), img->width, in);
        int padding_skipped = fseek(in, padding, SEEK_CUR);
        if (pixels_read != img->width || padding_skipped != 0) {
            free_image(img);
            return BMP_READ_DATA_CORRUPT;
        }
    }
    return BMP_READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (out == NULL || img == NULL) return BMP_WRITE_ARG_NULL;
    struct bmp_header header = create_bmp_header(img->width, img->height);
    size_t header_written = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (header_written != 1) return BMP_WRITE_OUT_OF_SPACE;
    uint8_t padding = row_padding(img->width);
    char padding_buffer[BMP_PADDING] = {0};
    for (uint32_t y = 0; y < img->height; y++) {
        struct pixel* pixel_row = &img->data[y * img->width];
        size_t pixels_written = fwrite(pixel_row, sizeof(struct pixel), img->width, out);
        size_t padding_pixels = fwrite(&padding_buffer, sizeof(char), padding, out);
        if (pixels_written != img->width || padding_pixels != padding) return BMP_WRITE_OUT_OF_SPACE;
    }
    return BMP_WRITE_OK;
}

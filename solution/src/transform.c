#include "../include/transform.h"
#include <stdlib.h>

struct pixel *pixel_at(struct image const *img, uint64_t x, uint64_t y) {
    return &img->data[y * img->width + x];
}

struct image rotated_img(struct image const img) {
    struct image rotated_img = create_image(img.height, img.width);
    if (rotated_img.data == NULL) return (struct image){0};
    for (uint64_t i = 0; i < img.height; i++)
        for (uint64_t j = 0; j < img.width; j++)
            *pixel_at(&rotated_img, img.height - i - 1, j) = *pixel_at(&img, j, i);
    return rotated_img;
}

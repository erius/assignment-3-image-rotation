#include "../include/image.h"

#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    struct pixel *pixels = malloc(width * height * sizeof(struct pixel));
    if (pixels == NULL) return (struct image){0};
    return (struct image){.width = width, .height = height, .data = pixels};
}

void free_image(struct image *const img) {
    if (img == NULL) return;
    free(img->data);
    img->data = NULL;
    img->width = 0;
    img->height = 0;
}

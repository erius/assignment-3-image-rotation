#ifndef BMP_IO_H
#define BMP_IO_H

#include "bmp.h"

void bmp_print_read_status(enum read_status status, char* file_name);

void bmp_print_write_status(enum write_status status, char* file_name);

#endif

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct image rotated_img(struct image const img);

#endif

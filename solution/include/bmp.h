#ifndef BMP_H
#define BMP_H

#include "image.h"

#include <stdio.h>

#define BMP_PADDING 4
#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_INFO_HEADER_SIZE 40
#define BMP_PLANES 1
#define BMP_BITS_COUNT 24
#define BMP_NO_COMPRESSION 0
#define BMP_PPM 0
#define BMP_CLR 0

struct __attribute__((packed)) bmp_header;

enum read_status { BMP_READ_OK = 0, BMP_READ_ARG_NULL, BMP_READ_INVALID_HDR, BMP_READ_OUT_OF_MEM, BMP_READ_DATA_CORRUPT };

enum write_status { BMP_WRITE_OK = 0, BMP_WRITE_ARG_NULL, BMP_WRITE_OUT_OF_SPACE };

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

#endif
